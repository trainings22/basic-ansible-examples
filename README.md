# Some basic Ansible examples

Some tiny ansible files to get started with SURF Research Cloud component creation.

## Useful links

### Ansible

- https://docs.ansible.com/ansible/latest/


### yaml

- https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html
- https://yaml-multiline.info/
- https://www.w3schools.io/file/yaml-introduction/


## How to call an Ansible Playbook in the local terminal

(sudo) ansible-playbook my-playbook.yml [--check, --extra-vars name=value ]

## Explore the Ansible playbook file itself

Look at "my-1-directory.yml" in this repository, for example.
Notice how it adheres to the yaml-format.

See how the way the playbook should run is specified in the header.
What is actually run is the list of "module" specifications in the "tasks:" list.


## Prepare your own catalog-item

- Display a list of catalog items with the arrow next to the catalog tab
- Clone the catalog-item "Ubuntu 20.04 (SUDO enabled)"
- Modify the new catalog item (check https://servicedesk.surf.nl/wiki/x/FAAQAQ for details or ask)
    - Append the component "SRC-External plugin" from the component list below to the two components already listed above.
    - Give the application its own name and finish the creation process.

## "Wrap" the playbook into a Research Cloud component.

- In the catalog creation menu select "Components"
- Click the green "+" on the upper right to create your own component. (Refer to https://servicedesk.surf.nl/wiki/x/FgAQAQ for details)
- Choose the file "my-1-directory.yml" in this repository to be used by the new component
- Finish the component creation process.

## Add the component to your prepared catalog-item

- Open your prepared catalog item again.
- You can now find your new component in the list of components below
- Append the new component to the list of components (**after** the "external" component)
- Finish the editing process of the catalog item.

## Run

- You can now find your new catalog item in your catalog overview
- Start a workspace using your new catalog item

## When your new catalog item succeeds ...

- In your component, change your file to "my-2-directory.yml" and add the required parameter to the component *or* to the catlog-item
    - What is the difference?
- Make "dir" an interactive parameter for the catalog-user to fill in when starting a workspace.

## ... otherwise we will learn together while debugging

## More Ansible

- Look up the "copy" module in the Ansible documentation (see link above).
- Copy /etc/hosts to the /tmp directory

- Look up the "lineinfile" module in the Ansible documentation.
- Append a comment line (starting with "#") to /etc/hosts

- Look up the "package" module in the Ansible documentation.
- Install a package of your choice
- What is the difference with the "apt" and "yum" modules? Why?

- Scim through the yaml-files in these repositories
    - https://gitlab.com/rsc-surf-nl/plugins/application-jupyter
    - https://gitlab.com/rsc-surf-nl/plugins/plugin-custom-packages

    - How can you detect which Linus distro is running?
    - How can you apply an "if" to a single task
    - How can you apply an "if" to a group of tasks?
    - Note how the implicit "files" directory works






